data class Person(var name: String, var address: String, var id: Int)
val person = Person("Jahnavi", "Guntur", 24)
fun main(args:Array<String>) {
    println(person.name)   //Jahnavi
    println(person.address) //Guntur
    println(person.id) //24
    println(person.toString())
}

