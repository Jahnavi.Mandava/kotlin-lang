open class Box {
    private var width: Double
    private var height: Double
    private var depth: Double

    constructor(ob: Box) {
        width = ob.width
        height = ob.height
        depth=ob.depth
    }
    constructor(w:Double,h:Double,d:Double){
        width=w
        height=h
        depth=d
    }
    constructor(){
        width=-1.0
        height=-1.0
        depth=-1.0
    }
    constructor(len:Double){
        width=len
        height=len
        depth=len
    }
    fun volume():Double{
        return width*height*depth
    }

}
open class BoxWeight : Box{
    var weight:Double
    constructor(ob:BoxWeight):super(ob){
        weight=ob.weight
    }
    constructor(w:Double,h:Double,d:Double,n:Double):super(w,h,d){
        weight=n
    }
    constructor():super(){
        weight=-1.0
    }
    constructor(len:Double,n:Double):super(len){
        weight=n
    }

}
class BoxCost : BoxWeight{
    var cost:Double
    constructor(ob:BoxCost):super(ob){
        cost=ob.cost
    }
    constructor(w:Double,h:Double,d:Double,n:Double,c:Double):super(w,h,d,n){
        cost=c
    }
    constructor():super(){
        cost=-1.0
    }
    constructor(len:Double,n:Double,c:Double):super(len,n){
        cost=c
    }
}
fun main(args:Array<String>){
    var b1=BoxCost(10.0,15.0,20.0,25.75,50.5)
    var b2=BoxCost(2.0,3.0,4.0,5.6,10.5)
    var vol:Double
    vol=b1.volume()
    println("volume of b1: "+vol)
    println("weight of b1: "+b1.weight)
    println("Cost Rs: "+b1.cost)
    println()
    vol=b2.volume()
    println("volume of b2: "+vol)
    println("weight of b2: "+b2.weight)
    println("Cost Rs: "+b2.cost)
    println()
}