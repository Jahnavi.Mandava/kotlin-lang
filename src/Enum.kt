fun main(args:Array<String>) {
    val direction1: Direction = Direction("NORTH", DirectionColor.RED)
    val direction2: Direction = Direction("SOUTH", DirectionColor.GREEN)

    println("The color of my " + direction1.name + " is " + direction1.color)
    println("The color of my " + direction2.name + " is " + direction2.color)


    println(direction1.color.toString() + " value is " + direction1.color.rgb)
    println(direction2.color.toString() + " value is " + direction2.color.rgb)
}
data class Direction(val name:String,val color:DirectionColor)

enum class DirectionColor(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}


