sealed class Operation {
    class Add(val value: Int) : Operation()
    class Substract(val value: Int) : Operation()

}
fun main(args:Array<String>){
   val operation:Operation=Operation.Substract(value = 4)
    val output = when (operation){
       is Operation.Add -> 4+operation.value
       is Operation.Substract -> 8-operation.value

   }
     println(output);
}
//sealed class Intention {
//    class Refresh : Intention()
//    class LoadMore : Intention()
//}
//fun main(args: Array<String>) {
//    val intention: Intention = Intention.LoadMore()
//    val output = when (intention) {
//        is Intention.Refresh -> "refresh"
//        is Intention.LoadMore -> "load more"
//    }
//    println(output)
//}
